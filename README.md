### Introduction ###

This is a manual, describing how to install and run Grafana, using the Docker container. 
We will also create a dashboard from TestData DB data source.


### Preparation ###

If you have Docker installed already, jump to section “Installation”. If not, let’s play with Docker stuff here for a while. 

Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. 
Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. 
More information about Docker, how it works and its' features you can find at this [link](https://docs.docker.com/engine/docker-overview)

1. Go to [Docker website](https://www.docker.com) and download Docker Desktop for your operating system. 
2. [Here](https://hub.docker.com/editions/community/docker-ce-desktop-windows) you can find more examples on how to check if Docker runs properly on **Windows**, 
   [follow](https://hub.docker.com/editions/community/docker-ce-desktop-mac) the same instructions for **MacOS**


### Installation ###

You can install and run **Grafana** using the official **Docker** container.
The official Grafana Docker image comes in two variants: Alpine Linux and Ubuntu. 
Alpine image is dedicated as the default image, and is [recommended by the Grafana team](https://grafana.com/docs/grafana/latest/installation/docker) due to security reasons and the smallest final image size.  

But you can use the Ubuntu image either. For this variant - follow the link above. 

1. Open **Terminal** app on your computer
2. Copy and paste this script to the command line. It will run the latest stable version of Grafana: 
`docker run -d -p 3000:3000 grafana/grafana`

You will see something like `Status: Downloaded...`


### Usage ###

To run Grafana web interface, open your browser and go to the default Grafana port: 3000. 
It is the default HTTP port that Grafana listens to if you haven't configured a different port.

For that just copy and paste this address to the entry line in your browser: `http://localhost:3000`

You will see a login page: 
![like this](https://imgur.com/8levBSh.png)

On this login page, type "**admin**" for both fields: username and password.

Then you will be asked to change your password: 

![and see this](https://i.imgur.com/Pw2ZtDDh.png)

And you will be redirected to the Homepage: 

![and see this](https://imgur.com/yCi0L0B.png)

Excellent job! Don’t forget to record your credentials!


### Enable the TestData DB source ###

Firstly, we have to enable TestData DB source. It is not enabled by default. The purpose of this data source is to make it easier to create fake data for any panel. 

Hover to **Configuration** menu (small gear icon on the left panel of the screen)

![as it shown](https://i.imgur.com/jcEVZWU.png?2)

Click on **Data Sources**

Click **Add Data Source**

![as it shown](https://imgur.com/9ZykgXC.png)

Search for **TestData DB** and press **Select**

![as it shown](https://i.imgur.com/buHliv9.png)

Press **Save & Test** to enable it

You will see the message that the data source is now working, congratulations!

Move to the **Homepage** for the next step: 

![like this](https://imgur.com/GVmxrDQ.png)


### Creating a dashboard ###

1. On the Homepage click **New dashboard**
2. Feel free to **Choose visualization** settings
3. Click **Add Query**. Grafana creates a basic graph panel with the Random Walk scenario. 
4. Make sure that you use the **TestData DB** source. It’s shown in the pop-up menu next to the **Query**

![result](https://imgur.com/X9JTtiQ.png)

That’s it! Don’t forget to **save** your dashboard!
Happy analyzing :) 


### Contributors ###
* Alina Terekhova 






